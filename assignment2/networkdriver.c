/* Kathryn Lovett */
/* DuckID: lovett2 */
/* CIS 415 Project 2: A PThread-based Network Driver */
/* This is my own work except that I referenced the example pthread code from the tutorial on Canvas. Also, I asked */
/* Professor Sventek about how to properly implement the receiver thread, in particular the backup packet descriptor */

#include <pthread.h>
#include <stdio.h>
#include "networkdriver.h"
#include "freepacketdescriptorstore.h"
#include "freepacketdescriptorstore__full.h"
#include "destination.h"
#include "diagnostics.h"
#include "BoundedBuffer.h"
#include "networkdevice.h"
#include "packetdescriptor.h"
#include "packetdescriptorcreator.h"

/* any global variables required for use by your threads
 * and your driver routines.
 */
static int size = MAX_PID+1;
BoundedBuffer *send_packet_buffer;
BoundedBuffer *get_packet_buffer[MAX_PID+1];
static NetworkDevice *nd;
static FreePacketDescriptorStore *fpds;

 /* definition[s] of function[s] required for your thread[s] */
static void* sending_nd() {
	while(1) {
		/* get packet from send buffer and send packet to network device */
		PacketDescriptor *pd = blockingReadBB(send_packet_buffer);
		int result = send_packet(nd, pd);
		if(result == 0) {
			result = send_packet(nd, pd);
		}
		/* return used packet to store */
		blocking_put_pd(fpds, pd);
	}
	return NULL;
}

static void* getting_nd() {
	/* Declare variables and get first packet descriptor from store */
	PacketDescriptor *pd;
	PID pd_pid;
	blocking_get_pd(fpds, &pd);

	for(;;) {
		/* Infinite loop that initializes a packet, registers it with the network device, */
		/* and awaits an incoming packet. Once it gets one, it attempts to get another packet */
		/* If successful, it writes to the bfufer, otherwise it tries again with the previous packet */
		PacketDescriptor *other_pd;
		init_packet_descriptor(pd);
		register_receiving_packetdescriptor(nd, pd);
		await_incoming_packet(nd);

		pd_pid = packet_descriptor_get_pid(pd);
		if(nonblocking_get_pd(fpds, &other_pd)) {
			if(nonblockingWriteBB(get_packet_buffer[pd_pid], pd)) {
				pd = other_pd;
			}
			else {
				nonblocking_put_pd(fpds, other_pd);
			}
		}
	}
	return NULL;
}

void blocking_send_packet(PacketDescriptor *pd) {
 	/* queue up packet descriptor for sending */
 	/* do not return until it has been successfully queued */
 	blockingWriteBB(send_packet_buffer, pd);
}

int nonblocking_send_packet(PacketDescriptor *pd) {
 	/* if you are able to queue up packet descriptor immediately, do so and return 1 */
 	/* otherwise, return 0 */
 	return nonblockingWriteBB(send_packet_buffer, pd);
}

void blocking_get_packet(PacketDescriptor **pd, PID pid) {
 	/* wait until there is a packet for 'pid' */
 	/* return that packet descriptor to the calling application */
 	*pd = blockingReadBB(get_packet_buffer[pid]);
}

int nonblocking_get_packet(PacketDescriptor **pd, PID pid) {
 	/* if there is currently a waiting packet for 'pid', return that packet */
 	/* to the calling application and return 1 for the value of the function */
 	/* otherwise, return 0 for the value of the function */
 	return nonblockingReadBB(get_packet_buffer[pid], (void**) pd);
}

void init_network_driver(NetworkDevice *nd_one, void *mem_start,
 	unsigned long mem_length, FreePacketDescriptorStore **fpds_one) {
	pthread_t send_network_device;
 	pthread_t receive_network_device;
 	int i;

 	/* create Free Packet Descriptor Store */
 	*fpds_one = create_fpds();
 	fpds = *fpds_one;
 	nd = nd_one;

 	/* load FPDS with packet descriptors constructed from mem_start/mem_length */
 	/* create any buffers required by your thread[s] */
 	create_free_packet_descriptors(fpds, mem_start, mem_length);

 	send_packet_buffer = createBB(size);
 	for(i = 0; i < size; i++) {
 		get_packet_buffer[i] = createBB(size);
 	}

 	/* create any threads you require for your implementation */
 	pthread_create(&send_network_device, NULL, &sending_nd, NULL);
 	pthread_create(&receive_network_device, NULL, &getting_nd, NULL);
 	/* return the FPDS to the code that called you */
}