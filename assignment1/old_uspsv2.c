#include "p1fxns.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <signal.h>

int SIZE = 100;
int INDEX;
int USR1_received = 0;
sigset_t signal_set;

void signal_handler(int signum) {
	if(signum == SIGUSR1) {
		USR1_received = 1;
	}
}

static char** getInput() {
	char **new = malloc(SIZE*sizeof(char *));
	INDEX = 0;
	char bf[2048];
	char newline = '\n';
	int j = 0;
	int i;
	for(i = 0; i < SIZE; i++) {
		new[i] = NULL;
	}

	while(p1getline(STDIN_FILENO, bf, sizeof(bf))) {

		j = p1strchr(bf, newline);
		bf[j] = '\0';
		new[INDEX] = p1strdup(bf);

		INDEX++;
	}

	return new;
}

static char** getArguments(char *args) {
	char **myArgs = malloc(10*sizeof(char *));
	char *temp = (char *)malloc(50*sizeof(char));
	int i;
	int j = 0;
	for(i = 0; i < 10; i++) {
		myArgs[i] = NULL;
	}

	i = 0;
	while(j > -1) {
		j = p1getword(args, j, temp);
		if(j > -1) {
			myArgs[i] = p1strdup(temp);
		}
		i++;
	}
	free(temp);
	return myArgs;
} 

static char*** getCommands(char **proc) {
	char ***args = malloc((INDEX+1)*sizeof(char **));
	char *temp;
	int i;

	for(i = 0; i < INDEX; i++) {
		if(proc[i] != NULL) {
			temp = p1strdup(proc[i]);
			args[i] = getArguments(temp);
			free(temp);
		}
	}
	return args;
}




static void delete(char **arr) {
	int i;
	for(i = 0; i < SIZE; i++) {
		free(arr[i]);
	}
	free(arr);
}

int main(int argc, char *argv[]) {
	int i;
	int j;
	char **proc = getInput();
	int pid[INDEX];

	char ***args = getCommands(proc);
	signal(SIGUSR1, &signal_handler);

    sigemptyset(&signal_set);
    sigaddset(&signal_set, SIGUSR1);
    // Do these need to be part of the set? Works without?
    sigaddset(&signal_set, SIGSTOP);
    sigaddset(&signal_set, SIGCONT);

	for(i = 0; i < INDEX; i++) {
		if(proc[i] != NULL) {
			pid[i] = fork();
			if(pid[i] == 0) {
				int signum = 0;
				while(!USR1_received) {
					printf("Waiting for signal\n");
					sigwait(&signal_set, &signum);
					signal_handler(signum);
					sleep(1);
				}
				printf("Unblocked!\n");
				printf("Executing command %s\n", args[i][0]);

				//Trying to do error checking - is this right?
				if(execvp(args[i][0], args[i]) == -1) {
					p1perror(STDOUT_FILENO, "execvp");
					return 1;
				}
			}
			else if(pid[i] < 0) {
				p1perror(STDOUT_FILENO, "fork");
				return 1;
			}
		}
	}

	for(i = 0; i < INDEX; i++) {
		//USPS parent process sends each program a SIGUSR1 signal to wake them up
		//Each process will then return from sigwait() and invoke execvp()
		//raise(SIGUSR1);
		printf("Sending signal SIGUSR1 to %d\n", pid[i]);
		kill(pid[i], SIGUSR1);
	}
	
	for(i = 0; i < INDEX; i++) {
		printf("Sending signal SIGSTOP to %d\n", pid[i]);
		kill(pid[i], SIGSTOP);
	}
	
	for(i = 0; i < INDEX; i++) {
		printf("Sending signal SIGCONT to %d\n", pid[i]);
		sleep(5);
		kill(pid[i], SIGCONT);
	}
	
	for(i = 0; i < INDEX; i++) {
		wait(&pid[i]);
	}
	
	//delete(program);
	//delete(args);
	for(i = 0; i < INDEX; i++) {
		for(j = 0; j < 10; j++) {
			free(args[i][j]);
		}
		free(args[i]);
	}
	free(args);

	delete(proc);
	
	return 0;
}