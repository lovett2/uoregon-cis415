#include "p1fxns.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <signal.h>

int SIZE = 100;
int INDEX;
int USR1_received = 0;
int running;
//int pid[100];
int cur;
sigset_t signal_set;

#define DEFAULT_PROCS 25
int nprocs = 0;
typedef struct proc {
	int pid;
	int finished;
	char *command;
	char **args;
} Proc;
Proc *procs = NULL;
int sprocs = -1;

/*
static void alarm_handler() {
	//will suspend the running workload process, 
	//determine the next workload process to run, and send it a SIGCONT signal,
	//reset the alarm, and continue with whatever else it is doing.
	
	if(pid[cur] > 0) {
		if(cur > 0 && cur < INDEX) {
			kill(pid[cur - 1], SIGSTOP);
			printf("Stopped %d and started %d\n", pid[cur - 1], pid[cur]);
		}
		kill(pid[cur], SIGCONT);
	}
	
	kill(pid[cur], SIGCONT);
	printf("Alarm called!\n");
}
*/
static void signal_handler(int signum) {
	if(signum == SIGUSR1) {
		USR1_received = 1;
	}
}

static char** getInput() {
	char **new = malloc(SIZE*sizeof(char *));
	INDEX = 0;
	char bf[2048];
	char newline = '\n';
	int j = 0;
	int i;
	for(i = 0; i < SIZE; i++) {
		new[i] = NULL;
	}

	while(p1getline(STDIN_FILENO, bf, sizeof(bf))) {

		j = p1strchr(bf, newline);
		bf[j] = '\0';
		new[INDEX] = p1strdup(bf);

		INDEX++;
	}

	return new;
}

static char** getArguments(char *args) {
	char **myArgs = malloc(10*sizeof(char *));
	char *temp = (char *)malloc(10*sizeof(char));
	int i;
	int j = 0;
	for(i = 0; i < 10; i++) {
		myArgs[i] = NULL;
	}

	i = 0;
	while(j > -1) {
		j = p1getword(args, j, temp);
		if(j > -1) {
			myArgs[i] = p1strdup(temp);
		}
		i++;
	}
	free(temp);
	return myArgs;
} 
/*
static char*** getCommands(char **proc) {
	char **args = malloc((INDEX+1)*sizeof(char **));
	char *temp;
	int i;

	for(i = 0; i < INDEX; i++) {
		if(proc[i] != NULL) {
			temp = p1strdup(proc[i]);
			args[i] = getArguments(temp);
			free(temp);
		}
	}
	return args;
}
*/
static void add_proc(int id, char *command_line) {
	if(sprocs < 0) {
		sprocs = DEFAULT_PROCS;
		procs = malloc(sprocs*sizeof(Proc));
	}
	else if(sprocs <= nprocs) {
		//realloc array of Proc structures of size sprocs + DEFAULT_PROCS
        //sprocs += DEFAULT_PROCS
        procs = realloc(procs, sprocs + DEFAULT_PROCS);
        sprocs += DEFAULT_PROCS;
	}
	//Fill in procs[nprocs]
	//procs[nprocs] = malloc(sizeof(Proc));
	procs[nprocs].pid = id;
	procs[nprocs].command = command_line;
	procs[nprocs].args = getArguments(command_line);

	nprocs++;
	printf("Num procs = %d\n", nprocs);
}


static void delete(char **arr) {
	int i;
	for(i = 0; i < SIZE; i++) {
		free(arr[i]);
	}
	free(arr);
}

int main(int argc, char *argv[]) {
	int i;
	int j;
	//char **proc = getInput();
	//int pid[INDEX];

	//char ***args = getCommands(proc);
	signal(SIGUSR1, &signal_handler);

    sigemptyset(&signal_set);
    sigaddset(&signal_set, SIGUSR1);
    // Do these need to be part of the set? Works without?
    sigaddset(&signal_set, SIGSTOP);
    sigaddset(&signal_set, SIGCONT);
    //signal(SIGALRM, &alarm_handler);
    char *command;
    char bf[2048];
    char newline = '\n';
    int pid = 0;
    j = 0;
    while(p1getline(STDIN_FILENO, bf, sizeof(bf))) {
    	//command = malloc(SIZE*sizeof(char));
		j = p1strchr(bf, newline);
		bf[j] = '\0';
		command = p1strdup(bf);
		pid = fork();

		if(pid < 0) {
			printf("Error on fork\n");
			p1perror(STDOUT_FILENO, "Fork");
			return 1;
		}
		else if(pid == 0) {
			// Child Process
			//kill(getpid(), SIGSTOP);
			int signum = 0;
			while(!USR1_received) {
				printf("Waiting for signal\n");
				sigwait(&signal_set, &signum);
				signal_handler(signum);
				sleep(1);
			}

			printf("Unblocked!\n");
			printf("Command: %s", command);
			char **args = getArguments(command);
			//Trying to do error checking - is this right?
			if(execvp(args[0], args) == -1) {
				p1perror(STDOUT_FILENO, "execvp");
				return 1;
			}
		}
		else if(pid > 0) {
			// Parent process
			add_proc(pid, command);
		}
	}

	// for(i = 0; i < nprocs; i++) {
	// 	printf("Command is: %s\n", procs[i].command);
	// }

	for(i = 0; i < nprocs; i++) {
		//USPS parent process sends each program a SIGUSR1 signal to wake them up
		//Each process will then return from sigwait() and invoke execvp()
		//raise(SIGUSR1);
		//printf("Sending signal SIGUSR1 to %d\n", pid[i]);
		kill(procs[i].pid, SIGUSR1);
	}
	
	for(i = 0; i < nprocs; i++) {
		//printf("Sending signal SIGSTOP to %d\n", pid[i]);
		kill(procs[i].pid, SIGSTOP);
	}
	
	// cur = 0;
	// while(cur != INDEX) {
	// 	alarm(1);
	// 	cur++;
	// }
	
	for(i = 0; i < nprocs; i++) {
		//printf("Sending signal SIGCONT to %d\n", pid[i]);
		sleep(5);
		kill(procs[i].pid, SIGCONT);
	}
	
	for(i = 0; i < nprocs; i++) {
		wait(&procs[i].pid);
	}

	for(i = 0; i < nprocs; i++) {
		free(procs[i].command);
		free(procs[i].args);
		//free(procs[i]);
	}
	free(procs);

	return 0;
}