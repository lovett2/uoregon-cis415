/* Kathryn Lovett
 * Spring 2016
 * CIS 415: Project 1 Part 2
 * This is my own work except that I referenced the code on Piazza
 * found here: https://piazza.com/class/il1f5y5z5ru6kw?cid=61
 * I also received help from Cathy Webster that looked at my code in
 * main - specifically the while loop.
 * Specifically for part 2, I looked at the child_handler.c code which
 * was included in LabFiles/lab4/project_1
 * I also received help from Manu, who looked at my code for me.
 */
#include "p1fxns.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <signal.h>

// Declaring global variables
#define DEFAULT_PROCS 25

 // nprocs tracks the number of processes
int nprocs = 0;
typedef struct proc {
	int pid;
	char *command;
} Proc;
// procs is an array of processes
// sprocs tracks the size of procs
Proc *procs = NULL;
int sprocs = -1;
char **args = NULL;
int USR1_received = 0;
#define UNUSED __attribute__ ((unused))


static void signal_handler(UNUSED int signum) {
	// Changes USR1_received to 1 when signalled
	USR1_received = 1;
}

static char** getArguments(char *args) {
	// Takes in char array and parses into individual
	// arguments, returning 2D array of args
	int i;
	int j = 0;
	char **myArgs = malloc(DEFAULT_PROCS*sizeof(char *));
	for(i = 0; i < DEFAULT_PROCS; i++) {
		myArgs[i] = NULL;
	}
	char *temp = (char *)malloc(DEFAULT_PROCS*sizeof(char));

	for(i = 0; i < DEFAULT_PROCS; i++) {
		myArgs[i] = NULL;
	}

	i = 0;
	while(j > -1) {
		j = p1getword(args, j, temp);
		if(j > -1) {
			myArgs[i] = p1strdup(temp);
		}
		i++;
	}
	if(temp != NULL) {
		free(temp);
	}
	return myArgs;
} 

static void add_proc(int id, char *command_line) {
	// Adds process to procs array, allocating space
	// for procs as needed
	if(sprocs < 0) {
		sprocs = DEFAULT_PROCS;
		procs = malloc(sprocs*sizeof(Proc));
	}
	else if(sprocs <= nprocs) {
		//realloc array of Proc structures of size sprocs + DEFAULT_PROCS
        procs = realloc(procs, (sprocs + DEFAULT_PROCS)*sizeof(Proc));
        sprocs += DEFAULT_PROCS;
	}
	//Fill in procs[nprocs]
	procs[nprocs].pid = id;
	procs[nprocs].command = command_line;

	nprocs++;
}

static void delete() {
	// Frees space allocated to procs
	int i;
	for(i = 0; i < nprocs; i++) {
		free(procs[i].command);
	}
	free(procs);
}


int main(UNUSED int argc, UNUSED char *argv[]) {
	// Takes in a line at a time, forking for each 
	// line and executing that line's command
	int i;
	int j = 0;
	char *command = NULL;
    char bf[2048];
    char newline = '\n';
    int pid = 0;
    if(signal(SIGUSR1, &signal_handler) == SIG_ERR) {
    	p1perror(STDOUT_FILENO, "Can't catch SIGUSR1\n");
    	exit(0);
    }

    while(p1getline(STDIN_FILENO, bf, sizeof(bf))) {
		j = p1strchr(bf, newline);
		bf[j] = '\0';
		command = p1strdup(bf);
		pid = fork();

		if(pid < 0) {
			p1perror(STDOUT_FILENO, "Fork");
			exit(0);
		}
		else if(pid == 0) {
			// Child Process
			// Children wait for SIGUSR1 signal from parent
			while(!USR1_received) {
				sleep(1);
			}

			args = getArguments(command);

			if(execvp(args[0], args) == -1) {
				p1perror(STDOUT_FILENO, "Execvp");
				exit(0);
			}
		}
		else if(pid > 0) {
			// Parent process
			add_proc(pid, command);
		}
		if(args != NULL) {
			for(i = 0; i < nprocs; i++) {
				free(args[i]);
			}
			free(args);
		}
	}
	// Parent signals SIGSTOP to children after SIGUSR1
	for(i = 0; i < nprocs; i++) {
		kill(procs[i].pid, SIGUSR1);
	}
	
	for(i = 0; i < nprocs; i++) {
		kill(procs[i].pid, SIGSTOP);
	}
	// Parent finally signals SIGCONT, allowing children to 
	// finish executing
	for(i = 0; i < nprocs; i++) {
		kill(procs[i].pid, SIGCONT);
	}
	// Parent waits until all children terminate before ending
	for(i = 0; i < nprocs; i++) {
		wait(&procs[i].pid);
	}

	delete();
	return 0;
}