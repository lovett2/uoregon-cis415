#include "p1fxns.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <signal.h>

int SIZE = 100;
int INDEX;
int USR1_received = 0;
sigset_t signal_set;

void signal_handler(int signum) {
	if(signum == SIGUSR1) {
		USR1_received = 1;
	}
}

static char** getInput() {
	char **new = malloc(SIZE*sizeof(char *));
	INDEX = 0;
	char bf[2048];
	char newline = '\n';
	int j = 0;
	int i;
	for(i = 0; i < SIZE; i++) {
		new[i] = NULL;
	}

	while(p1getline(STDIN_FILENO, bf, sizeof(bf))) {

		j = p1strchr(bf, newline);
		bf[j] = '\0';
		//new[INDEX] = malloc(p1strlen(bf)*sizeof(char));
		new[INDEX] = p1strdup(bf);

		INDEX++;
	}

	return new;
}

static char** getArguments(char *args) {
	char **myArgs = malloc(10*sizeof(char *));
	char *temp = (char *)malloc(50*sizeof(char));
	int i;
	for(i = 0; i < 10; i++) {
		myArgs[i] = NULL;
	}

	int j = 0;
	//int newline = 0;
	i = 0;
	while(j > -1) {
		j = p1getword(args, j, temp);
		if(j > -1) {
			// Don't need to allocate cause strdup does (?)
			//myArgs[i] = malloc(p1strlen(args)*sizeof(char));
			myArgs[i] = p1strdup(temp);
			//newline = p1strchr(myArgs[i], '\n');
			//myArgs[i][newline] = '\0';
		}
		i++;
	}
	free(temp);
	return myArgs;
} 

static char*** getCommands(char **proc) {
	char ***args = malloc((INDEX+1)*sizeof(char **));
	char *temp;
	int i;

	for(i = 0; i < INDEX; i++) {
		if(proc[i] != NULL) {
			temp = p1strdup(proc[i]);
			args[i] = getArguments(temp);
			free(temp);
		}
	}
	return args;
}




static void delete(char **arr) {
	int i;
	for(i = 0; i < SIZE; i++) {
		free(arr[i]);
	}
	free(arr);
}

int main(int argc, char *argv[]) {
	int i;
	int j;
	//int k;
	char **proc = getInput();
	int pid[INDEX];
	/*
	char ***args = malloc((SIZE)*sizeof(char **));
	
	for(i = 0; i < SIZE; i++) {
		args[i] = malloc(10*sizeof(char *));
		for(k = 0; k < 10; k++) {
			args[i][k] = NULL;
		}
	}
	
	getCommands(proc, args);
	*/

	char ***args = getCommands(proc);
	signal(SIGUSR1, &signal_handler);

	//sigset_t sigset;
    sigemptyset(&signal_set);
    sigaddset(&signal_set, SIGUSR1);
    //sigaddset(&signal_set, SIGSTOP);
    //sigaddset(&signal_set, SIGCONT);
    //sigprocmask(SIG_BLOCK, &signal_set, NULL);

	for(i = 0; i < INDEX; i++) {
		if(proc[i] != NULL) {
			pid[i] = fork();
			if(pid[i] == 0) {
				int signum = 0;
				printf("Waiting fur signal\n");
				int result = sigwait(&signal_set, &signum);
				signal_handler(signum);
				
				while(!USR1_received) {
					sleep(1);
				}
				if(result == 0) {
					printf("Unblocked!\n");
					printf("Executing command %s\n", args[i][0]);
					execvp(args[i][0], args[i]);
				}
				
				//if(execvp(args[i][0], args[i]) == -1) {
				//	p1perror(STDOUT_FILENO, "execvp");
				//}
			}
			else if(pid[i] < 0) {
				p1perror(STDOUT_FILENO, "fork");
				return 1;
			}
		}
	}
/*
	for(i = 0; i < INDEX; i++) {
		wait(&pid[i]);
	}
*/
	for(i = 0; i < INDEX; i++) {
		//USPS parent process sends each program a SIGUSR1 signal to wake them up
		//Each process will then return from sigwait() and invoke execvp()
		//raise(SIGUSR1);
		printf("Sending signal SIGUSR1 to %d\n", pid[i]);
		kill(pid[i], SIGUSR1);
	}
	/*
	for(i = 0; i < INDEX; i++) {
		printf("Sending signal SIGSTOP to %d\n", pid[i]);
		kill(pid[i], SIGSTOP);
	}

	for(i = 0; i < INDEX; i++) {
		printf("Sending signal SIGCONT to %d\n", pid[i]);
		kill(pid[i], SIGCONT);
		//wait(&pid[i]);
	}
	*/
	for(i = 0; i < INDEX; i++) {
		wait(&pid[i]);
	}
	
	//delete(program);
	//delete(args);
	for(i = 0; i < INDEX; i++) {
		for(j = 0; j < 10; j++) {
			free(args[i][j]);
		}
		free(args[i]);
	}
	free(args);

	delete(proc);
	
	return 0;
}