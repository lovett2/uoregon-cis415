#include "p1fxns.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>

int SIZE = 100;
int INDEX;

static char** getInput() {
	char **new = malloc(SIZE*sizeof(char *));
	INDEX = 0;
	char bf[2048];
	char newline = '\n';
	int j = 0;
	int i;
	for(i = 0; i < SIZE; i++) {
		new[i] = NULL;
	}

	while(p1getline(STDIN_FILENO, bf, sizeof(bf))) {

		j = p1strchr(bf, newline);
		bf[j] = '\0';
		new[INDEX] = p1strdup(bf);

		INDEX++;
	}

	return new;
}

static char** getArguments(char *args) {
	char **myArgs = malloc(10*sizeof(char *));
	char *temp = (char *)malloc(50*sizeof(char));

	int i;
	for(i = 0; i < 10; i++) {
		myArgs[i] = NULL;
	}

	int j = 0;
	i = 0;
	while(j > -1) {
		j = p1getword(args, j, temp);
		if(j > -1) {
			myArgs[i] = p1strdup(temp);
		}
		i++;
	}
	free(temp);
	return myArgs;
} 

static char*** getCommands(char **proc) {
	char ***args = malloc((INDEX+1)*sizeof(char **));
	char *temp;
	int i;
	for(i = 0; i < INDEX; i++) {
		if(proc[i] != NULL) {
			temp = p1strdup(proc[i]);
			args[i] = getArguments(temp);
			free(temp);
		}
	}
	return args;
}

static void deleteArgs(char ***arr) {
	int i;
	int j;
	for(i = 0; i < INDEX; i++) {
		for(j = 0; j < 10; j++) {
			free(arr[i][j]);
		}
		free(arr[i]);
	}
	free(arr);
}


static void deleteInput(char **arr) {
	int i;
	for(i = 0; i < SIZE; i++) {
		free(arr[i]);
	}
	free(arr);
}

int main(int argc, char *argv[]) {
	int i;
	int j;
	char **proc = getInput();
	int pid[INDEX];

	char ***args = getCommands(proc);

	for(i = 0; i < INDEX; i++) {
		if(proc[i] != NULL) {
			pid[i] = fork();
			if(pid[i] == 0) {
				if(execvp(args[i][0], args[i]) < 0) {
					p1perror(STDOUT_FILENO, "Execvp");
				}
			}
		}
		
	}

	for(i = 0; i < INDEX; i++) {
		wait(&pid[i]);
	}

	deleteArgs(args);
	deleteInput(proc);
	
	return 0;
}