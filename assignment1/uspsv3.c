/* Kathryn Lovett
 * Spring 2016
 * CIS 415: Project 1 Part 3
 * This is my own work except that I referenced the code on Piazza
 * found here: https://piazza.com/class/il1f5y5z5ru6kw?cid=61
 * I also received help from Cathy Webster that looked at my code in
 * main - specifically the while loop.
 * I referenced my queue code from the queue code found on Canvas
 * under Files/JavaToC.pdf
 * I referenced child_handler.c on Canvas for my alarm_handler
 */
#include "p1fxns.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <signal.h>
#define DEFAULT_PROCS 25
#define UNUSED __attribute__ ((unused))
// Declaring global variables
int nprocs = 0;
typedef struct proc {
	int pid;
	int status;
	char *command;
} Proc;

typedef struct queue_element {
	struct queue_element *next;
	Proc *myproc;
} QueueElement; 

typedef struct queue {
	struct queue_element *first; 
	struct queue_element *last; 
	int count;
} Queue; 
// Queue to hold processes yet to finish executing
Queue *que;
// Tracks the currently executing process
Proc *curElem = NULL;
char **args = NULL;
int USR1_received = 0;
int dead_processes = 0;

static Queue *create(void) {
	// Creates a new queue, initializing first, last, and count
	Queue *rq = (Queue *)malloc(sizeof(Queue));
	if(rq != NULL) { 
		rq->first = NULL; 
		rq->last = NULL; 
		rq->count = 0; 
	} 
	return rq;
}

static int enqueue(Queue *q, Proc *d) {
	// Adds process d to Queue q
	// Allocates space for new QueueElement
	QueueElement *pr = (QueueElement *)malloc(sizeof(QueueElement)); 
	if(pr == NULL) {
		return 0;
	}
 	pr->next = NULL;
 	pr->myproc = d;
 	if(q->count++ == 0) {
 		q->first = pr;
 	}
 	else {
 		q->last->next = pr;
 	}
 	q->last = pr;
 	return 1;
}

static Proc* dequeue(Queue *q) { 
	// Removes first in item from Queue q
	// Returns the data (Proc) from that QueueElement
	QueueElement *pr;
	Proc *myproc;
	if(q->count == 0) {
		return NULL;
	}
	pr = q->first;
	myproc = pr->myproc;
	q->first = pr->next;
	free((void *)pr);
	if(--q->count == 0) {
		q->last = NULL;
	}

	return myproc;
}

static void alarm_handler(UNUSED int signum) {
	// Suspends the running workload process, 
	// determines the next workload process to run, and sends it 
	// a SIGCONT signal or SIGUSR1 depending on its status
	// Resets the alarm, and continue with whatever else it is doing
	if(curElem != NULL) {
		kill(curElem->pid, SIGSTOP);
		enqueue(que, curElem);
	}
	curElem = dequeue(que);
	if(curElem != NULL) {
		if(curElem->status == 1) {
			kill(curElem->pid, SIGCONT);
		}
		else {
			curElem->status++;
			kill(curElem->pid, SIGUSR1);
		}
		alarm(1);
	}
}

static void signal_handler(UNUSED int signum) {
	USR1_received = 1;
}

static void child_handler(UNUSED int signum) {
	// Receives SIGCHLD signal from child processes
	// When child has terminated, increments dead_processes
	// and frees curElem (dead process), and sets it to NULL
	pid_t pid;
	int status;

  	while((pid = waitpid(-1, &status, WNOHANG)) > 0) {
		dead_processes++;
		break;
  	}
  	if(curElem != NULL) {
  		free(curElem);
  		curElem = NULL;
  	}
}

static char** getArguments(char *args) {
	// Takes in char array and parses for individual arguments
	// Returns 2D array of arguments for commands
	char **myArgs = malloc(DEFAULT_PROCS*sizeof(char *));
	char *temp = (char *)malloc(DEFAULT_PROCS*sizeof(char));
	if(myArgs == NULL || temp == NULL) {
		p1perror(STDOUT_FILENO, "Malloc");
		exit(0);
	}
	int i;
	int j = 0;
	for(i = 0; i < DEFAULT_PROCS; i++) {
		myArgs[i] = NULL;
	}

	i = 0;
	while(j > -1) {
		j = p1getword(args, j, temp);
		if(j > -1) {
			myArgs[i] = p1strdup(temp);
		}
		i++;
	}
	if(temp != NULL) {
		free(temp);
	}
	return myArgs;
} 

static void add_proc(int id, char *command_line) {
	// Takes in pid and command and adds new process
	// to Queue of processes, incrementing the nprocs
	Proc *newproc = malloc(sizeof(Proc));
	if(newproc == NULL) {
		p1perror(STDOUT_FILENO, "Malloc");
		exit(0);
	}
	newproc->pid = id;
	newproc->command = command_line;
	newproc->status = 0;
	if(enqueue(que, newproc) == 0) {
		p1perror(STDOUT_FILENO, "Enqueue");
		exit(0);
	}
	nprocs++;
}

int main(UNUSED int argc, UNUSED char *argv[]) {
	// Takes in commands a line at a time, forks creating
	// a new child, and then makes each child wait for 
	// the SIGUSR1 signal
	int j, i;
	char *command;
    char bf[2048];
    char newline = '\n';
    int pid = 0;
    if(signal(SIGALRM, &alarm_handler) == SIG_ERR) {
    	p1perror(STDOUT_FILENO, "Can't catch SIGALRM\n");
    	exit(0);
    }
    if(signal(SIGCHLD, &child_handler) == SIG_ERR) {
    	p1perror(STDOUT_FILENO, "Can't catch SIGCHLD\n");
    	exit(0);
    }
    if(signal(SIGUSR1, &signal_handler) == SIG_ERR) {
    	p1perror(STDOUT_FILENO, "Can't catch SIGUSR1\n");
    	exit(0);
    }
    
    j = 0;
    que = create();
    while(p1getline(STDIN_FILENO, bf, sizeof(bf))) {
		j = p1strchr(bf, newline);
		bf[j] = '\0';
		command = p1strdup(bf);
		pid = fork();

		if(pid < 0) {
			p1perror(STDOUT_FILENO, "Fork");
			exit(0);
		}
		else if(pid == 0) {
			// Child Process
			while(!USR1_received) {
				sleep(1);
			}

			args = getArguments(command);
			if(execvp(args[0], args) == -1) {
				p1perror(STDOUT_FILENO, "Execvp");
				exit(0);
			}
		}
		else if(pid > 0) {
			// Parent process
			add_proc(pid, command);
			free(command);
		}
		if(args != NULL) {
			for(i = 0; i < nprocs; i++) {
				free(args[i]);
			}
			free(args);
		}
	}
	
	// Initialize alarm, starting the signalling and 
	// waking up of the child processes
	alarm(1);

	// Wait until dead_processes equals the number 
	// of processes (nprocs) before terminating parent
	while(dead_processes < nprocs) {
		sleep(1);
	}
	// Free the queue
	free(que);
	return 0;
}