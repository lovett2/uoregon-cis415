/* Kathryn Lovett
 * Spring 2016
 * CIS 415: Project 1 Part 1
 * This is my own work except that I referenced the code on Piazza
 * found here: https://piazza.com/class/il1f5y5z5ru6kw?cid=61
 * I also received help from Cathy Webster that looked at my code in
 * main - specifically the while loop.
 */
#include "p1fxns.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <signal.h>

// Declaration of global variables
#define DEFAULT_PROCS 25

// nprocs holds the total number of processes
int nprocs = 0;
// struct proc holds info for each process
typedef struct proc {
	int pid;
	char *command;
} Proc;
// array of processes. sprocs is the size of the procs array
Proc *procs = NULL;
int sprocs = -1;
char **args = NULL;
#define UNUSED __attribute__ ((unused))


static char** getArguments(char *args) {
	// Takes in array of command and breaks up into
	// individual arguments, returning a 2D array of args
	char **myArgs = malloc(DEFAULT_PROCS*sizeof(char *));
	char *temp = (char *)malloc(DEFAULT_PROCS*sizeof(char));
	int i;
	int j = 0;
	for(i = 0; i < DEFAULT_PROCS; i++) {
		myArgs[i] = NULL;
	}

	i = 0;
	while(j > -1) {
		j = p1getword(args, j, temp);
		if(j > -1) {
			myArgs[i] = p1strdup(temp);
			i++;
		}
	}
	free(temp);
	return myArgs;
} 

static void add_proc(int id, char *command_line) {
	// Adds process to array of processes, allocating
	// memory for it and assigning the structs qualities 
	if(sprocs < 0) {
		sprocs = DEFAULT_PROCS;
		procs = malloc(sprocs*sizeof(Proc));
	}
	else if(sprocs <= nprocs) {
		//realloc array of Proc structures of size sprocs + DEFAULT_PROCS
        procs = realloc(procs, (sprocs + DEFAULT_PROCS)*sizeof(Proc));
        sprocs += DEFAULT_PROCS;
	}
	//Fill in procs[nprocs]
	procs[nprocs].pid = id;
	procs[nprocs].command = command_line;

	nprocs++;
}

static void delete() {
	// Frees space allocated to processes array
	int i;
	for(i = 0; i < nprocs; i++) {
		free(procs[i].command);
	}
	free(procs);
}


int main(UNUSED int argc, UNUSED char *argv[]) {
	// Takes in line, forks, has child process execute
	// that command
	// Waits for all children to terminate then ends program
	int i;
	int j = 0;
    char *command;
    char bf[2048];
    char newline = '\n';
    int pid = 0;

    while(p1getline(STDIN_FILENO, bf, sizeof(bf))) {
		j = p1strchr(bf, newline);
		bf[j] = '\0';
		command = p1strdup(bf);
		pid = fork();

		if(pid < 0) {
			p1perror(STDOUT_FILENO, "Fork");
			exit(0);
		}
		else if(pid == 0) {
			// Child Process
			args = getArguments(command);
			if(execvp(args[0], args) == -1) {
				p1perror(STDOUT_FILENO, "Execvp");
				return 1;
			}
		}
		else if(pid > 0) {
			// Parent process
			add_proc(pid, command);
		}
		if(args != NULL) {
			for(i = 0; i < nprocs; i++) {
				free(args[i]);
			}
			free(args);
		}
	}
	
	for(i = 0; i < nprocs; i++) {
		wait(&procs[i].pid);
	}

	delete();
	return 0;
}