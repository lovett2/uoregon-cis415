/* Kathryn Lovett
 * Duck ID: lovett2
 * CIS 415 Project 0
 * This is my own work.
 */
#include "date.h"
#include <time.h>
#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>

// Initializing struct date
struct date {
	int day;
	int month;
	int year;
};

Date *date_create(char *datestr) {
	// Allocating memory to Date instance input
	Date *input = malloc(sizeof(struct date));
	assert(input != NULL);

	// Parsing input datestr and saving 
	// the first part to input->day, the second
	// to input->month, and the third to input->year
	char str[80];
	strcpy(str, datestr);
	const char s[2] = "/";
	char *token;
	token = strtok(str, s);

	int counter = 0;
	while(token != NULL) {
		if(counter == 0) {
			sscanf(token, "%d", &input->day);
		}
		else if(counter == 1) {
			sscanf(token, "%d", &input->month);
		}
		else {
			sscanf(token, "%d", &input->year);
		}
		token = strtok(NULL, s);
		counter++;
	}
	
	return input;
}

Date *date_duplicate(Date *d) {
	// Allocates memory to new copy of Date d, transfers
	// data from d to copy and then returns copy
	Date *copy = malloc(sizeof(struct date));;
	copy->day = d->day;
	copy->month = d->month;
	copy->year = d->year;

	return copy;
}

int date_compare(Date *date1, Date *date2) {
	// Compares date1 and date2 first by year,
	// then if year is the same, by month, then 
	// if month is the same, by day. Returns
	// -1 if date1 is less than date2, 1 if it is 
	// greater than date2 adn 0 if they're equal
	if(date1->year < date2->year) {
		return -1;
	}
	else if(date1->year > date2->year) {
		return 1;
	}
	else {
		if(date1->month < date2->month) {
			return -1;
		}
		else if(date1->month > date2->month) {
			return 1;
		}
		else {
			if(date1->day < date2->day) {
				return -1;
			}
			else if(date1->day > date2->day) {
				return 1;
			}
			else {
				return 0;
			}
		}
	}
}

void date_destroy(Date *d) {
	assert(d != NULL);

	free(d);
}
