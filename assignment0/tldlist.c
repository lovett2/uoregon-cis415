/* Kathryn Lovett
 * Duck ID: lovett2
 * CIS 415 Project 0
 * This is my own work except that I reference the code here for my
 * AVL tree implementation: https://gist.github.com/tonious/1377768
 * In addition, I discussed the implementation of tldlist_destory and tldlist_iter_create
 * with Cathy Webster.
 */
#include "tldlist.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "date.h"

// Global variable to track index for TLDIterator
int indexElem = 0;

// Initializing struct tldlist
struct tldlist {
	Date *begin;
	Date *end;
	TLDNode *root;
	long total;
};

// Initializing struct tldnode
struct tldnode {
	long count;
	char name[5];
	TLDNode *leftChild;
	TLDNode *rightChild;
	int height;
};

// Initializing struct tlditerator
struct tlditerator {
	long next;
	long size;
	void **elements;
};

TLDList *tldlist_create(Date *begin, Date *end) {
	// Allocating memory to new TLDList newList
	// Initializes begin date to begin, end date to end,
	// root to NULL and the total number of successful additions
	// to newList to 0
	TLDList *newList = (TLDList *)malloc(sizeof(struct tldlist));
	
	if(newList != NULL) {
		newList->begin = begin;
		newList->end = end;
		newList->root = NULL;
		newList->total = 0;
	}

	return newList;
}

static TLDNode *createTLDNode() {
	// Allocating memory to new TLDNode newNode
	// Initalizes leftChild and rightChild to NULL, and the
	// number of instances count to 1
	TLDNode *newNode = NULL;
	newNode = malloc(sizeof(struct tldnode));

	if(newNode != NULL) {
		newNode->leftChild = NULL;
		newNode->rightChild = NULL;
		newNode->count = 1;
		newNode->height = 1;
		strcpy(newNode->name, "");
	}
	
	return newNode;
}

long tldlist_count(TLDList *tld) {
	// Returns 0 if root is NULL, else returns total additions to list
	if(tld->root == NULL) {
		return 0;
	}
	else {
		return tld->total;
	}
	
}

static TLDNode *rotateLL(TLDNode *node) {
 	/* Left Left Rotate */
 	TLDNode *a = node;
	TLDNode *b = a->leftChild;
	
	a->leftChild = b->rightChild;
	b->rightChild = a;

	a->height = a->height - 1;

	return(b);
}

static TLDNode *rotateLR(TLDNode *node) {
	/* Left Right Rotate */
	TLDNode *a = node;
	TLDNode *b = a->leftChild;
	TLDNode *c = b->rightChild;
	
	a->leftChild = c->rightChild;
	b->rightChild = c->leftChild; 
	c->leftChild = b;
	c->rightChild = a;

	a->height = a->height - 2;
	b->height = b->height - 1;
	c->height = c->height + 1;

	return(c);
}

static TLDNode *rotateRL(TLDNode *node) {
	/* Right Left Rotate */
	TLDNode *a = node;
	TLDNode *b = a->rightChild;
	TLDNode *c = b->leftChild;
	
	a->rightChild = c->leftChild;
	b->leftChild = c->rightChild; 
	c->rightChild = b;
	c->leftChild = a;

	a->height = a->height - 2;
	b->height = b->height - 1;
	c->height = c->height + 1;

	return(c);
}

static TLDNode *rotateRR(TLDNode *node) {
	/* Right Right Rotate */
	TLDNode *a = node;
	TLDNode *b = a->rightChild;
	
	a->rightChild = b->leftChild;
	b->leftChild = a; 

	a->height = a->height - 2;

	return(b);
}

static int getHeight(TLDNode *node) {
	// If node is NULL, return 0, else return its height
	if(node != NULL) {
		return node->height;
	}
	else {
		return 0;
	}
}

static TLDNode *balanceNode(TLDNode *node) {
	TLDNode *newRoot = NULL;

	// Balance children, if they exist - call function on children recursively
	if(node->leftChild != NULL) {
		node->leftChild = balanceNode(node->leftChild);
	}
	if(node->rightChild != NULL) {
		node->rightChild = balanceNode(node->rightChild);
	}

	// Check height difference of left and right children to determine whether they
	// need to be balanced
	int heightDiff = getHeight(node->leftChild) - getHeight(node->rightChild);
	if(heightDiff >= 2) {
		/* Left Heavy */
		int childDiff = getHeight(node->leftChild->leftChild) - getHeight(node->leftChild->rightChild);
		if(childDiff <= -1) {
			newRoot = rotateLR(node);
		}
		else {
			newRoot = rotateLL(node);
		}
	}
	else if(heightDiff <= -2) {
		/* Right Heavy */
		int childDiff = getHeight(node->rightChild->leftChild) - getHeight(node->rightChild->rightChild);
		if(childDiff >= 1) {
			newRoot = rotateRL(node);
		}
		else {
			newRoot = rotateRR(node);
		}
	}
	else {
		/* Node is balanced -- no change */
		newRoot = node;
	}
	return newRoot;
}

static TLDNode *updateHeight(TLDNode *node) {
	// Updates height of node, balancing the AVL tree as it goes
	if(node->leftChild == NULL && node->rightChild == NULL) {
		// Base case - if no children, height is one
		node->height = 1;
	}
	else {
		if(node->leftChild != NULL) {
			// If left child, call updateHeight recursively on left child, then
			// update the current node with the left child's height plus one
			node->leftChild = updateHeight(node->leftChild);
			node->height = node->leftChild->height + 1;
			node = balanceNode(node);
		}
		if(node->rightChild != NULL) {
			// If right child, call updateHeight recursively on right child, then,
			// if the right child's height plus one is greater than the nodes' current
			// height, change it to that
			node->rightChild = updateHeight(node->rightChild);
			int rightTreeHeight = node->rightChild->height + 1;
			if(rightTreeHeight > node->height) {
				node->height = rightTreeHeight;
			}
			node = balanceNode(node);
		}
	}
	return node;
}

int tldlist_add(TLDList *tld, char *hostname, Date *d) {
	// Adds new domain to TLDList tld - if successful, returns 1, else 0
	// Checks if new hostname is within the tld's begin and end date
	if(date_compare(d, tld->begin) >= 0 && date_compare(tld->end, d) >= 0) {
		// If within time period, parses hostname for domain name

		char str[80];
		strcpy(str, hostname);
		int i = 0;
		while(str[i]) {
			str[i] = tolower(str[i]);
			i++;
		}

		const char s[2] = ".";

		char *token;
		token = strtok(str, s);


		char *temp_tld;
		while(token != NULL) {
			temp_tld = token;
			token = strtok(NULL, s);
		}

		TLDNode *temp_node = NULL;
		if(tld->root == NULL) {
			// If root is null, create a new TLDNode and assign
			// it's name to the domain retreived above
			temp_node = createTLDNode();
			strcpy(temp_node->name, temp_tld);
			tld->root = temp_node;
		}
		else {
			TLDNode *next = NULL;
			TLDNode *last = NULL;
			TLDNode *node = NULL;
			next = tld->root;

			// Looks through the next node, comparing the new domain
			// to existing domains, until it finds a NULL node
			while(next != NULL) {
				last = next;
				if(strcmp(temp_tld, next->name) < 0) {
					next = next->leftChild;
				}
				else if(strcmp(temp_tld, next->name) > 0) {
					next = next->rightChild;
				}
				else if(strcmp(temp_tld, next->name) == 0) {
					next->count += 1;
					tld->total +=1;
					return 1;
				}
			}
			node = createTLDNode();
			strcpy(node->name, temp_tld);
			
			// Inserts new domain into TLDList 
			if(strcmp(temp_tld, last->name) < 0) {
				last->leftChild = node;
			}
			else if(strcmp(temp_tld, last->name) > 0) {
				last->rightChild = node;
			}
		}
		// Increments total additions to list
		tld->total += 1;
		
		// Updates height of nodes in list, balancing AVL tree as it goes
		tld->root = updateHeight(tld->root);
		return 1;
	}
	else {
		return 0;
	}
}

static void nodeDFS(TLDNode *node, void **elem) {
	// Adds item to elements of iterator, incrementing the index
	// Calls recursively on node's left and right children
	elem[indexElem] = node;
	indexElem += 1;

	if(node->leftChild) {
		nodeDFS(node->leftChild, elem);
	}

	if(node->rightChild) {
		nodeDFS(node->rightChild, elem);
	}
}

TLDIterator *tldlist_iter_create(TLDList *tld) {
	// Allocates memory to new TLDIterator it 
	// Assigns next to 0, its size to the total additons plus one, its
	// elements list to NULL, then allocates memory to it
	// Lastly, calls depth first search on the root in order to insert
	// nodes into elements list
	TLDIterator *it = (TLDIterator *)malloc(sizeof(TLDIterator));
	if(it != NULL) {
		it->next = 0L;
		it->size = tld->total + 1;
		it->elements = NULL;
		it->elements = malloc(it->size*sizeof(TLDList));

		int i = 0;
		while(i < it->size) {
			it->elements[i] = NULL;
			i++;
		}

		nodeDFS(tld->root, it->elements);
	}
	return it;
}

TLDNode *tldlist_iter_next(TLDIterator *iter) {
	// Returns the next node in iter's list of elements
	if(iter->next < iter->size) {
			long n = iter->next;
			iter->next++;
			return iter->elements[n];
	}
	else {
		return NULL;
	}
}

static void traverse(TLDNode *node) {
	// Goes through each node in AVL tree, checks for left and right children
	// and frees each node
	if(node->leftChild) {
        traverse(node->leftChild);
    } 
    if(node->rightChild) {
        traverse(node->rightChild);
    }
    if(node != NULL) {
        free(node);
    }
}

void tldlist_destroy(TLDList *tld) {
	traverse(tld->root);
	free(tld);
}

void tldlist_iter_destroy(TLDIterator *iter) {
	free(iter->elements);
	free(iter);
}

char *tldnode_tldname(TLDNode *node) {
	return node->name;
}

long tldnode_count(TLDNode *node) {
	return node->count;
}
